# rpi2-fedora-imgbuilder-settings

This repository contains my settings for building a usable image of Fedora Linux
for the Raspberry Pi 2.

To use this:

1. Clone this repository
    * `git clone --recursive https://bitbucket.org/Conan_Kudo/rpi2-fedora-imgbuilder-settings.git`

2. Make sure you have an 8GB microSD card
    * If you have a smaller/larger card, adjust `builder-settings/settings.conf` appropriately

3. Copy the configuration files into the `rpi2-fedora-image-builder` folder:
    * `cp -R builder-settings/* rpi2-fedora-image-builder`

4. Change into the `rpi2-fedora-image-builder` directory and build the image as root:
    * `cd rpi2-fedora-image-builder && sudo ./mkimage.sh`

5. Flash the created image to the SD card:
    * `dd if=<created.raw.img> of=</dev/sdX> bs=1M`


